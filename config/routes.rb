Rails.application.routes.draw do
  get 'reports', to: 'reports#index'
  
  get 'exportar', to: 'reports#exportar'
  post 'exportar', to: 'reports#exportar'

  get 'calendar', to:'calendar#index'

  resources :registers
  resources :users
  resources :people
  resources :roles

  get 'menu', to: 'menu#index' #Mostrar el menú
  get 'login', to: 'sessions#new' #Mostrar el formulario
  post 'login', to: 'sessions#create' #Crear la sesión
  get 'logout', to: 'sessions#destroy'
  
  post 'users/habilitar'
  post 'users/deshabilitar'
  post 'registers/query'
  get 'descargar', to: 'registers#descargar'
  
  get 'menu/index'
  root 'home#index'

  get '/exportar.xml', :to => 'reports#exportar', :defaults => {:format => 'xml'}

  
  get "reports/query"
  post "reports/query"
  
 

  match '/exportar.csv' => 'reports#exportar', via: :get, defaults: { format: :csv }

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
