class RegistersController < ApplicationController
  before_action :set_register, only: %i[ show edit update destroy ]

  # GET /registers or /registers.json
  def index
    @registers = Register.all
  end

  # GET /registers/1 or /registers/1.json
  def show
  end

  # GET /registers/new
  def new
    @register = Register.new
    @session = session[:user_id]
    @register.id = @session
  end

  # GET /registers/1/edit
  def edit
  end

  # POST /registers or /registers.json
  def create
    #FECHA
    @day = register_params["date(1i)"].to_i 
    @month = register_params["date(2i)"].to_i
    @year = register_params["date(3i)"].to_i
    @start_date = Date.new(@day, @month, @year)
    
    #HORA 
    @hour = register_params["hour(4i)"].to_i 
    @seg = register_params["hour(5i)"].to_i
    @start_time = Time.new(@day,@month,@year, @hour, @seg, 0)
    
    
    if register_params[:tipo] == "E"
      @register = Register.new

      @register.date = @start_date
      @register.hour_start = @start_time
      @register.hour_end = nil
      @register.total_hours = nil
      @register.user_id = register_params[:user_id]

    else
      puts "SALIDA"
      #Agregar llenar la hora en el hour_end

      @registers = Register.joins(:user).where(  users: {id: register_params[:user_id]} , registers:{date: @start_date.all_day})
      
      if @registers.length == 1

        @registers.each do |item|
          @register = Register.find(item.id)
          @register.hour_end = @start_time
          @register.hour_start  = item.hour_start
          @register.total_hours = (Time.parse(@register.hour_end.strftime("%Y-%m-%d %H:%M")) - Time.parse(@register.hour_start.strftime("%Y-%m-%d %H:%M")))/3600

        end
      else
        puts "NO HAY DATOS"
      end

    end
    
    respond_to do |format|
      if @register.save
        format.html { redirect_to @register, notice: "Register was successfully created."}
        format.json { render :show, status: :created, location: @register }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @register.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /registers/1 or /registers/1.json
  def update
    respond_to do |format|
      if @register.update(register_params)
        format.html { redirect_to @register, notice: "Register was successfully updated." }
        format.json { render :show, status: :ok, location: @register }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @register.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /registers/1 or /registers/1.json
  def destroy
    @register.destroy
    respond_to do |format|
      format.html { redirect_to registers_url, notice: "Register was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_register
      @register = Register.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def register_params
      params.require(:register).permit(:tipo,:hour,:date , :user_id)
      #params.require(:register).permit(:tipo, :hour,  :date, :hour_start, :hour_end, :total_hours, :user_id)
    end
end
