class ReportsController < ApplicationController

  
  def index
    @years = (2018..2021)
    @people = Person.all
    @months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre" ]
  end

  def query 

    @month = Integer(params[:month_id])
    @year = Integer(params[:year_id])
    @person_id = Integer(params[:person_id])

    @range = Date.new(@year, @month)
    
    user = User.where(person_id: @person_id)
    @user_id = user[0].id
    listado(@user_id,@range)


    respond_to do |format|
      format.html 
    end
    
  end
  
  def listado(user_id,range)
    @registers =  Register.where(  user_id: user_id, date: range.all_month )
    
    @registers.each do |item|
      puts "ITEM: #{item.date}"
    end

  end

    
  def exportar
    puts ":D"
    @range = Date.new(Integer(params[:year]), Integer(params[:month]))
    
    @registers = Register.filtrar(params[:user_id], @range)
    
    respond_to do |format|
      format.html
      format.csv {send_data @registers.to_csv}
      format.xls 
      format.xlm  { render :xml => @registers }   
    end

  end

end
