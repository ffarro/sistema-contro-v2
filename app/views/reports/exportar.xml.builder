xml.instruct!
xml.registers do
  @registers.each do |register|
    xml.register do
      xml.id register.id
      xml.date register.date
    end
  end
end