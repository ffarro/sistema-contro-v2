json.extract! user, :id, :username, :state, :role_id, :person_id, :created_at, :updated_at
json.url user_url(user, format: :json)
