class Person < ApplicationRecord
    has_one :user


    def full_name
        "#{self.try(:name)} #{self.try(:paternalLastName)} #{self.try(:maternalLastName)}"
    end
    
end
