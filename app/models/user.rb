class User < ApplicationRecord
  belongs_to :role
  belongs_to :person
  has_secure_password
end
