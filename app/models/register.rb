class Register < ApplicationRecord
  belongs_to :user

  def self.filtrar(user, range)
    Register.where(  user_id: user, date: range.all_month )
  end

  def self.to_csv
    CSV.generate() do |csv|
      csv << ["ID", "Empleado", "Fecha", "Hora de entrada", "Hora de salida", "Total de horas de trabajo"]
      all.each do |r|
        csv << [r.id, "#{r.user.person.name}" + " " + "#{r.user.person.paternalLastName}" +  " " + "#{r.user.person.maternalLastName}" , r.date, r.hour_start.strftime("%I:%M %p") , r.hour_end.strftime("%I:%M %p") , r.total_hours ]
      end
    end
  end

  
  #def self.to_csv
  #  CSV.generate() do |csv|
  #    csv << column_names
  #    all.each do |register|
  #      csv << register.attributes.values_at(*column_names)
  #    end
  #  end
  #end
  
end
