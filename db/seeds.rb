
Role.destroy_all
Person.destroy_all
User.destroy_all
Register.destroy_all



roles = Role.create([{ 
    description: 'administrador' 
}, 
{ 
    description: 'trabajador' 
}
])

#Person nombres:string apellidoPat:string apellidoMat:string dni:string 

people = Person.create([{ 
    name: 'Gabriel',
    paternalLastName: 'García',
    maternalLastName: 'Marquez'
}, 
{ 
    name: 'Lu',
    paternalLastName: 'Godoy',
    maternalLastName: 'Alcayaga'
},
{ 
    name: 'Julio',
    paternalLastName: 'Florencio',
    maternalLastName: 'Cortázar'
},
{ 
    name: 'Ricardo',
    paternalLastName: 'Palma',
    maternalLastName: 'Soriano'
}
])


users = User.create([{ 
    username: 'admin',
    password: '123',
    password_confirmation:'123',
    state: true,
    role_id: 1,
    person_id:1
},
{   
    username: 'user1',
    password: '123',
    password_confirmation:'123',
    state: true,
    role_id: 2,
    person_id:2
},
{ 
    username: 'user2',
    password: '123',
    password_confirmation:'123',
    state: true,
    role_id: 2,
    person_id:3
},
{ 
    username: 'user3',
    password: '123',
    password_confirmation:'123',
    state: true,
    role_id: 2,
    person_id:4
}
])


30.times do |i|
    Register.create(   
        date: "#{i+1}-09-2021",
        hour_start: ("#{i+1}-09-2021 8:00"),
        hour_end: ("#{i+1}-09-2021 13:00"),
        total_hours: 5,
        user_id: 2)
end

30.times do |i|
    Register.create(   
        date: "#{i+1}-09-2021",
        hour_start: ("#{i+1}-09-2021 8:00"),
        hour_end: ("#{i+1}-09-2021 13:00"),
        total_hours: 5,
        user_id: 3)
end

30.times do |i|
    Register.create(   
        date: "#{i+1}-09-2021",
        hour_start: ("#{i+1}-09-2021 8:00"),
        hour_end: ("#{i+1}-09-2021 13:00"),
        total_hours: 5,
        user_id: 4)
end

